Gâteau aux abricots
===================

image::img/gateau-abricots.jpg[Gâteau aux abricots]

Pour 8 personnes (moule de 26 cm de diamètre) :

Ingrédients
-----------
* 3 oeufs
* 150g de sucre
* 2 sachets de sucre vanillé
* 250g de farine
* 1 paquet de levure chimique
* 75g d'huile (amande, noix ou tournesol)
* 8-10 abricots
* sucre en grains (pour la décoration)

Préparation
-----------
. Battre les oeufs avec le sucre et le sucre vanillé jusqu'à ce que le mélange
  blanchisse et devienne mousseux.
. Ajouter l'huile bien mélanger.
. Ajouter ensuite la farine et la levure et mélanger avec un fouet.
. Verser la préparation dans un moule beurré.
. Laver les abricots puis les couper en quartiers.
. Répartir les quartiers d'abricots sur la pâte.
. Saupoudrer de sucre en grains.
. Enfourner dans le four préchauffé à 180°C pendant 45 minutes environ.
. Laisser tiédir avant de démouler.