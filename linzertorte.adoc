Linzer Torte
============

_(Tarte de linz)_

Ingrédients
-----------

* beurre: 250gr
* amandes moulues: 200gr (avec peau)
* farine: 200gr
* sucre: 200gr
* confiture: 200gr ou + (framboise, prune...)
* oeufs: 2
* sel: 1 pincée
* cannelle: 2 cuillères à café

Préparation
-----------
. Faire bien ramollir le beurre auparavant en le laissant dehors du frigo (4
heures devrait suffire). Battre le beurre avec le sucre jusqu'à obtenir
une mousse claire, rajouter les oeufs et ensuit touts les ingrédients secs
mélangés ensemble auparavant. Mettre de coté un peu de pâte pour la déco.
. Étaler la plus grande partie de la pâte dans une moule (26-28 cm) et
recouvrir de confiture.
. Pour le grillage: étaler la pâte mis de coté sur un
hauteur de 4mm environ et mettre au congélateur pendant 20 min.
. Sortir la pâte endurcie et couper des bandes de 6-8mm d’épaisseur. Décorer.
. Four chaud 180°C pendant 40 minutes au milieu.
