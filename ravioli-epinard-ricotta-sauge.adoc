= Ravioli aux épinards et ricotta avec beurre à la sauge

Pour 4 personnes.

== Ingrédients

* épinard: ~400gr (frais)
* ricotta: 300gr
* parmesan: 50gr ou plus suivant le goût

== Préparation

. Cuire les épinards à la vapeur, laisser refroidir, essorer à la main, jusqu'à
  que ça donne une boule assez sèche.
. Hacher les grossièrement.
. Mélanger avec la ricotta, le parmesan, sel, poivre, et noix de muscat,
  jusqu'à ce que la farce soit homogène.
. Préparer la pâte comme dans la recette des cappellettis.
. Confectionner les raviolis.

=== Beurre à la sauge

. Environ 120 g de beurre, feuilles de sauge hachées grossièrement.
. Chauffer tout à feu moyen-doux et laisser frire la sauge pendant environ 5
  minutes jusqu'à ce qu'elles deviennent presque croquantes. Le beurre ne doit
  pas brûler, mais c'est bon, quand il commence à se dorer très légèrement.

=== Raviolis

. Cuire les raviolis à feu doux pendant 2-3 minutes.
. Les sortir de l'eau délicatement et ajouter au beurre.
. Servir avec quelques écailles de parmesan et un peu de poivre.
