Gâteau Chocolat & Mascarpone
============================

- préparation: 15 minutes
- cuisson: 25 minutes

Ingrédients
-----------
* chocolat noir patissier: 200gr
* farine: 40gr
* sucre glace: 75gr
* mascarpone: 250gr
* oeuf: 4

Pour le glaçage
~~~~~~~~~~~~~~~
* chocolat noir patissier: 100gr
* beurre: 50gr

Préparation
-----------

. Préchauffez le four à th. 6/ – 200 °C. Chemisez un plat à four rectangulaire
  de papier sulfurisé. Cassez le chocolat en petits morceaux dans une casserole,
  (ajoutez 4 cuillerées à soupe d’eau) et faites fondre à feu doux en remuant
  jusqu’à obtention d’une pâte lisse.
. Versez le chocolat dans un saladier, ajoutez le mascarpone et mélangez.
  Incorporez les œufs entiers, le sucre glace et la farine. Versez la pâte dans
  le plat. Enfournez pour 25 minutes.
. Préparez le glaçage 10 minutes avant la fin de la cuisson du gâteau : dans une
  casserole, faites fondre à feu doux le chocolat cassé en morceaux avec le
  beurre coupé en lamelles. Mélangez jusqu’à obtenir une crème homogène.
. Versez le glaçage sur le gâteau et laissez durcir avant de démouler.
. Servir froid.

Notes
-----
Rajoutez des morceaux de poire et de noix de pécan.
