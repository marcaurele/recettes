= Pici

Pour 4 personnes, préparation 30 minutes, repos 30 minutes, cuisson 2 minutes.

== Ingrédients

* farine de blé dur: 200gr
* farine de blé normale (blanche, bise ou intégrale): 200gr
* oeuf: 1
* sel: 3/4 de cuillère à thé
* huile d'olive: 1 cuillère à soupe
* eau: environ 200ml

== Préparation

. Mélanger tous les ingrédients et pétrir la pâte à la main jusqu'à ce qu'elle
  devient lisse et souple et qu'elle ne colle plus aux doigts. Ajouter de la
  farine si nécessaire.
. Mettre à reposer sous un bol pendant une demi-heure.
. Prendre un tiers de la pâte et l'étaler à une épaisseur d'environ 1 cm. Ne
  pas mettre trop de farine ou de semoule : il vaut mieux que la pâte reste un
  peu collante pour pouvoir rouler les pici plus facilement.
. Couper des bandes d'environ 1 cm. Les rouler depuis le milieu vers
  l'extérieur en les tirant en longueur, comme si l'on faisait de gros
  spaghettis.
. Une fois roulés, les saupoudrer d'un peu de semoule. Les laisser séparés et
  ne pas faire des nids car ils ont tendance à coller ensemble.
. Dans une grande casserole d'eau salée, cuire pendant 2 minutes environ.

== Notes

. Certaines sauces s'adaptent mieux à un type de pâtes qu'à un autre, selon
  leurs textures. On privilégiera par exemple une sauce liquide et légère pour
  les pâtes creuses, une sauce crémeuse pour les pâtes longues et lisses, une
  sauce plus copieuse pour les pâtes larges ou poreuses (tagliatelle, pici)...
. En Italie, les pâtes sont versées dans la sauce et non le contraire.
  Mélangées plusieurs fois à feu très doux, les pâtes s'imprègnent ainsi des
  saveurs de la sauce.
. Ne jamais lésiner sur l'huile d'olive. La générosité est une caractéristique
  de la cuisine italienne — quand il faut, il faut !
