Confiture Abricots
===================

Simplement des notes pour les quelques détails que j'oublie.

Ingrédients
-----------
* abricots: 2.5kg
* sucre: 1kg
* citron: 1

Préparation
-----------
. Nettoyer les abricots.
. Couper en petits morceaux dans une grande poele.
. Rajouter le sucre pour un taux de 40-50% de la masse d'abricot.
. Presser le citron et le rajouter en mélangeant le tout.
. Laisser ~12h ou plus pour faire sortir le jus des abricots.
. Faire chauffer à feu moyen + (6-7) pendant 1h-1h30 jusqu'à ce que la goutte
  tienne à la cuillère.
. Mettre en pot, et miam miam!
