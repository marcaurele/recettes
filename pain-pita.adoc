Pain pita
=========

Pour 4 pains.

Ingrédients
-----------
* 200 gr farine
* 1 càc sel
* 1/2 càc sucre
* 1 càs huile d'olive
* 110 ml eau tiède
* 1 càc levure boulangère sèche

Préparation
-----------
. Mélanger l'eau tiède et la levure boulangère sèche et laisser reposer 10
  minutes.
. Mettre la farine, le sel, le sucre et l'huile d'olive dans un bol.
. Ajouter le mélange eau + levure boulangère sèche dans le bol. Pétrir à vitesse
  2 jusqu'à ce que la pâte se décolle. Laisser reposer 1 heure minimum à
  température ambiante.
. Dégazer la pâte puis diviser la pâte en quatre pâtons égaux (environ 85 gr) et
  étaler ces pâtons.
. Faire cuire les pains pita dans une poêle chaude à feu moyen pendant environ
  3 minutes de chaque côté.
