= Pesto tomates séchées et noix

Pour un bocal ~220gr, préparation 20 minutes, repos 1 heure.

== Ingrédients

* tomate séchées à l'huile d'olive: 100gr
* cerneaux de noix: 80gr
* parmesan frais: 30gr
* huile d'olive: 4 cuillères à soupe
* persil frais
* poivre

== Préparation

. Couper les tomates, les noix et le parmesan en petits morceaux.
. Ajouter l'huile d'olive, mettre le tout dans un bol et passer au mixer jusqu'à
  ce que la texture soit onctueuse.
. Laisser reposer une heure pour que le pesto développe tous ses arômes.

== Notes

. Servir avec de la focaccia, du pain grillé ou des pâtes.
. Le pesto se conserve jusqu'à 5 jours au frigo.
